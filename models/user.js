'use strict';
const bcrypt = require("bcrypt");
const {SALT_ROUNDS} = require('../middleware/config');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    phone_number: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: { 
        len: {
          args: [7, 20],
          msg: "Phone number invalid, too short."
        },
        isNumeric: {
          msg: "Not valid phone number."
        } 
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    freezeTableName: true,
    underscored: true
  });
  User.associate = function(models) {
    User.belongsTo(models.User_role);
  };
  
  User.addHook("beforeCreate", function(user) {
    return bcrypt.hash(user.password, SALT_ROUNDS).then(hashedPw => {
      user.password = hashedPw;
    });
  });

  User.addHook("beforeUpdate", function(user) {
    return bcrypt.hash(user.password, SALT_ROUNDS).then(hashedPw => {
      user.password = hashedPw;
    });
  });

  User.prototype.verifyPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
  }

  return User;
};