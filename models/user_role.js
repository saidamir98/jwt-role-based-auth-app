'use strict';
module.exports = (sequelize, DataTypes) => {
  const User_role = sequelize.define('User_role', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    role: DataTypes.STRING,
    permission: DataTypes.STRING
  }, {
    freezeTableName: true,
    underscored: true
  });
  User_role.associate = function(models) {
    // associations can be defined here
  };
  return User_role;
};