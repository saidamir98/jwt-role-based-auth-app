# NodeJs JWT Role Based Auth-App

## Description
NodeJs JWT Role-based auth middleware for Restify API applications using express.js and sequelize. This is in the MVC format,
except because it is an API there are no views, just models and controllers.

source code can be found here: https://gitlab.com/saidamir98/jwt-role-based-auth-app.git
##### Routing         : Express
##### ORM Database    : Sequelize
##### Authentication  : Passport, JWT

## Installation

#### Download Code | Clone the Repo

```
git clone {repo_name}
```

#### Install Node Modules
```
npm install
```

#### Configure config.* files
You will find a config.json file in the config directory and config.js file in the middleware directory. Change the variables to fit your application

#### Run Seeders
```
node_modules/.bin/sequelize db:seed:all
```