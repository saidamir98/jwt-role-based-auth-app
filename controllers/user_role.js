const _ = require('lodash');
const User_role = require('../models').User_role;

const CONTAINER = ['role', 'permission'];
module.exports = {
    list(req, res) {
        return User_role.findAll()
        .then(result => res.status(200).send(result))
        .catch(error => res.status(400).send(error));
    },
    
    getById(req, res) {
        return User_role.findById(req.params.id)
        .then((result) => {
            if (!result) {
                return res.status(404).send({
                    message: 'User_role Not Found',
                });
            }
            return res.status(200).send(result); 
        })
        .catch(error => res.status(400).send(error));
    },

    add(req, res) {
        let body = _.pick(req.body, CONTAINER);
        return User_role.create(body)
        .then(result => res.status(201).send(result))
        .catch(error => res.status(400).send(error));
    },
    
    update(req, res) {
        return User_role.findById(req.params.id)
        .then((result) => {
            if(!result) {
                return res.status(404).send({
                    message: 'User_role Not Found',
                }); 
            }
            let body = _.pick(req.body, CONTAINER);
            return result.update(body)
            .then(()=> res.status(200).send(result))
            .catch(error => res.status(400).send(error));
        })
    },
    
    delete(req, res) {
        return User_role.findById(req.params.id)
        .then((result) => {
            if (!result) {
                return res.status(400).send({
                    message: 'User_role Not Found',
                });
            }
            return result.destroy()
            .then(()=> res.status(204).send())
            .catch((error) => res.status(400).send(error));
        })
        .catch(error => res.status(400).send(error));
    }
};