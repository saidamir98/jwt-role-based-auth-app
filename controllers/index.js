const user_role = require('./user_role');
const user = require('./user');

module.exports = {
    user_role,
    user
}