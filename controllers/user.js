const _ = require('lodash');
const User = require('../models').User;
const {JWT_SECRET} = require('../middleware/config');
const jwt = require('jsonwebtoken');

const CONTAINER = ['phone_number', 'password', 'user_role_id'];
module.exports = {
    me(req, res) {
        let token = req.header('x-auth');
        const decoded = jwt.verify(token, JWT_SECRET);
        User.findById(decoded.id).then(user=>{
            res.status(200).send(user);
        })
        .catch(error => res.status(400).send(error));
    },
    
    list(req, res) {
        return User.findAll()
        .then(result => res.status(200).send(result))
        .catch(error => res.status(400).send(error));
    },
    
    getById(req, res) {
        return User.findById(req.params.id)
        .then((result) => {
            if (!result) {
                return res.status(404).send({
                    message: 'User Not Found',
                });
            }
            return res.status(200).send(result); 
        })
        .catch(error => res.status(400).send(error));
    },

    add(req, res) {
        let body = _.pick(req.body, CONTAINER);
        return User.create(body)
        .then(result => res.status(201).send(result))
        .catch(error => res.status(400).send(error));
    },
    
    update(req, res) {
        return User.findById(req.params.id)
        .then((result) => {
            if(!result) {
                return res.status(404).send({
                    message: 'User Not Found',
                }); 
            }
            let body = _.pick(req.body, CONTAINER);
            return result.update(body)
            .then(()=> res.status(200).send(result))
            .catch(error => res.status(400).send(error));
        })
    },
    
    delete(req, res) {
        return User.findById(req.params.id)
        .then((result) => {
            if (!result) {
                return res.status(400).send({
                    message: 'User Not Found',
                });
            }
            return result.destroy()
            .then(()=> res.status(204).send())
            .catch((error) => res.status(400).send(error));
        })
        .catch(error => res.status(400).send(error));
    }
};