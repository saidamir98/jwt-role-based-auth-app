const express = require('express');
const router = express.Router();
const controllers = require('../controllers');
const {hasRoles} = require('../middleware/accessController');

const userRoleController = controllers.user_role;
router.get('/user_role', hasRoles(['super_admin', 'admin']), userRoleController.list);
router.get('/user_role/:id', hasRoles(['super_admin', 'admin']), userRoleController.getById);
router.post('/user_role', hasRoles('super_admin'), userRoleController.add);
router.put('/user_role/:id', hasRoles('super_admin'), userRoleController.update);
router.delete('/user_role/:id', hasRoles('super_admin'), userRoleController.delete);

const userController = controllers.user;
router.get('/me', userController.me);
router.get('/user', hasRoles('admin'), userController.list);
router.get('/user/:id', hasRoles('admin'), userController.getById);
router.post('/user', hasRoles('super_admin'), userController.add);
router.put('/user/:id', hasRoles('super_admin'), userController.update);
router.delete('/user/:id', hasRoles('super_admin'), userController.delete);

module.exports = router;