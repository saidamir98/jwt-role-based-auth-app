const express = require('express');
const router = express.Router();

const jwt = require('jsonwebtoken');
const passport = require('passport');
const CFG = require("./config.js");

router.post('/login', function (req, res, next) {
    passport.authenticate('local', {session: false}, (error, user, info) => {
        let message = info.message;
        if (error || !user) {
            return res.status(400).json({
                message: message,
                user: user
            });
        }
        req.login(user, {session:false}, (error) => {
            if (error) {
                res.status(400).send(error);
            }
            const jwtPayload = {
                id: user.id,
                phone: user.phone_number,
                user_role_id: user.user_role_id
            }
            const token = jwt.sign(jwtPayload, CFG.JWT_SECRET);
            return res.status(200).json({
                token: token,
                jwtPayload: jwtPayload,
                message: message
            });
        });
    })(req, res, next);
});

const _ = require('lodash');
const User = require('../models').User;
router.post('/register', function (req, res) {
    let body = _.pick(req.body, ['phone_number', 'password']);
    User.create(body)
    .then(result => res.status(201).send(result))
    .catch(error => res.status(400).send(error));
});

module.exports = router;

