const User_role = require('../models').User_role;
const {JWT_SECRET} = require('./config');
const jwt = require('jsonwebtoken');
function hasRoles(roles = []) {
    return hasRoles[roles] || (hasRoles[roles] = function(req, res, next) {
        if (typeof roles === 'string') {
            roles = [roles];
        }
        let token = req.header('x-auth');
        const decoded = jwt.verify(token, JWT_SECRET);
        if (decoded.user_role_id!=null) {
            User_role.findById(decoded.user_role_id).
            then(user_role => {
                if (roles.indexOf(user_role.role)<0) {
                    res.status(401).send({message: 'Access Denied'});
                } else {
                    next();
                }
            })
            .catch(error => res.status(400).send(error));    
        } else {
            res.status(401).send({message: 'No Access Right'});
        }
    });
}

module.exports = {
    hasRoles
}