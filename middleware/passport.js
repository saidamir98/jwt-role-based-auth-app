const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require('passport-jwt');
const User = require('../models').User; // call user model
const CFG = require("./config.js");
const ExtractJwt = passportJWT.ExtractJwt;
const JWTStrategy = passportJWT.Strategy;

passport.use(new LocalStrategy({
    usernameField: 'phone_number',
    passwordField: 'password'
}, function (phone, password, next) {
    return User.findOne({where: {phone_number: phone}})
    .then(user => {
        if (!user || !user.verifyPassword(password)) {
            return next(null, false, {message: 'Incorrect phone number or password'});
        }
        return next(null, user, {message: 'Logged In Siccessfully'});
    })
    .catch(error => {
        return next(error);
    });
}));

passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('x-auth'),
    secretOrKey: CFG.JWT_SECRET
}, (jwtPayload, next) => {
    return User.findById(jwtPayload.id)
    .then(user => {
        if (jwtPayload.iat + CFG.JWT_EXPIRATION < Date.now()/1000) {
            return next('jwt expired', null);
        }
        return next(null, user);
    })
    .catch(error => {
        return next(error, null);
    });
}));
