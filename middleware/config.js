module.exports = {  
    JWT_SECRET: "MyS3cr3tK3Y",
    JWT_EXPIRATION: 3600,
    SALT_ROUNDS: 10
};