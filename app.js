const express = require('express');
const logger = require('morgan');
const bodyParser= require('body-parser');
const passport = require('passport');
const auth = require('./middleware/auth');
const app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

app.get("/", (req, res) => {
    return res.status(200).send({status:"success", message:"Parcel Pending API", data:{}});
});

require('./middleware/passport');
app.use('/', auth);

const v1 = require('./routes/v1');
app.use('/', passport.authenticate('jwt', {session: false}), v1);



const models = require("./models");
models.sequelize.authenticate().then(() => {
    models.sequelize.sync(/*{force:true}*/).then(()=>{
        console.log('Connected to the database');
    });
}).catch(err => {
    console.error('Unable to connect to the database:', err);
});

module.exports = app;
