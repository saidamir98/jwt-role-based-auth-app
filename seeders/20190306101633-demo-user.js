'use strict';
const bcrypt = require("bcrypt");
const {SALT_ROUNDS} = require('../middleware/config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('User', [{
      phone_number: '998998465798',
      password: bcrypt.hashSync('password', SALT_ROUNDS),
      user_role_id: 1,
      created_at: 'now()',
      updated_at: 'now()'
    }, {
      phone_number: '998943614198',
      password: bcrypt.hashSync('password', SALT_ROUNDS),
      user_role_id: 2,
      created_at: 'now()',
      updated_at: 'now()'
    }, {
      phone_number: '998987654321',
      password: bcrypt.hashSync('password', SALT_ROUNDS),
      user_role_id: 3,
      created_at: 'now()',
      updated_at: 'now()'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('User_role', null, {});
  }
};
