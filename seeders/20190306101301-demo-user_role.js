'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('User_role', [{
      role: 'super_admin',
      permission: 'all',
      created_at: 'now()',
      updated_at: 'now()'
    }, {
      role: 'admin',
      permission: 'dashboard',
      created_at: 'now()',
      updated_at: 'now()'
    }, {
      role: 'customer',
      permission: 'shop',
      created_at: 'now()',
      updated_at: 'now()'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('User_role', null, {});
  }
};
